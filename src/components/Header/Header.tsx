import React from 'react';
import { Link } from 'react-router-dom';
import styles from './style.module.css';

const NAV_LABELS: string[] = ['home', 'news'];

function Header() {
    return (
        <nav className={styles.navigation}>
            <ul className={styles.navList}>
                {NAV_LABELS.map((label) => <li className={styles.listItem} key={label}>
                    <Link className={styles.link} to={label}>
                        {label}
                    </Link>
                </li>)}
            </ul>
        </nav>
    )
}

export default Header;