import React from 'react';
import styles from './style.module.css';

const NewsItem = ({data} : {data:any}) => {
    console.log(data)
    return(
        <div className={styles.itemContainer}>
            <div>
                <img className={styles.image} src={data?.urlToImage} alt="news" />
            </div>
            <div className={styles.content}>
                <h3>{data?.title}</h3>
                <p>{data.content}</p>
            </div>
        </div>
    )
}

export default NewsItem;