import React from 'react';
import { Outlet } from 'react-router-dom';
import Header from '../Header/Header';
import styles from './style.module.css';

function Layout() {
    return (
        <>
            <Header />
            <div className={styles.container}>
                <Outlet />
            </div>
        </>
    )
}

export default Layout;