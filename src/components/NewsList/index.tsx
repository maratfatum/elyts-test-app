import React from 'react';
import uuid from 'react-uuid';
import useNewData from '../../hooks/useNewsData';
import NewsItem from '../NewsItem';
import styles from './style.module.css';

function NewsList() {
    const { data, isLoading, isError } = useNewData();

    if(isError) {
        <h2>Something was wrong!</h2>
    }

    if (isLoading) {
        return <h2>Loading...</h2>
    }

    return (
        <div className={styles.newsContainer}>
            {data?.articles?.map((item:any)=> <NewsItem data={item} key={uuid()}/>)}
        </div>
    )
}

export default NewsList;