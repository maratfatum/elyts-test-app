import axios from "axios";

const API_KEY = '9a4dcb089a1c45ed9b0b05c5c0843694';

const axiosInstanse = axios.create({
    baseURL: "https://newsapi.org/v2/",
    headers: {
        'X-Api-Key': API_KEY,
    }
})

export const api = {
    getNews: () => axiosInstanse.get(`top-headlines?country=us`)
}