import {Routes, Route} from 'react-router-dom';
import Layout from './components/Layout';
import HomePage from './pages/HomePage';
import NewsPage from './pages/NewsPage';

function App() {
  return (
    <Routes>
      <Route path="/" element={<Layout/>}>
        <Route path="/home" element={<HomePage/>}/>
        <Route path="/news" element={<NewsPage/>}/>
      </Route>
    </Routes>
  );
}

export default App;
