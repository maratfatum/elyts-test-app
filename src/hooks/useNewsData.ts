import { useQuery } from "react-query";
import { api } from "../services/api";

const getNews = async () => {
    const result  = await api.getNews();
    if(result.status !== 200) {
        throw new Error('err')
    }
    return result.data;
}

const useNewData = () => {
    const { data, isLoading, isError } = useQuery('news', getNews,
        {
            refetchOnWindowFocus: false,
        }
    );

    return { data, isLoading, isError };
}

export default useNewData;